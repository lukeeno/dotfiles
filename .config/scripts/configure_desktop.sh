#!/usr/bin/bash

### Variables
full_path=$(realpath $0)
script_dir=$(dirname $full_path)
dir=$(dirname $script_dir)


### Presymlink
rm -rf $HOME/.oh-my-zsh/ $HOME/.zshrc
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# zsh plugins
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-completions ${ZSH_CUSTOM:=~/.oh-my-zsh/custom}/plugins/zsh-completions
git clone https://github.com/TamCore/autoupdate-oh-my-zsh-plugins ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/autoupdate


### Symlink config
# vim
ln -sf $dir/.vimrc $HOME/.vimrc

# zsh
ln -sf $dir/.zshrc $HOME/.zshrc

# gitconfig
ln -sf $dir/.gitconfig $HOME/.gitconfig

