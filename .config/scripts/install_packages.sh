### Install
# internal
sudo pacman -S intel-ucode sudo udiskie

# base
sudo pacman -S base-devel vim zsh ranger mc openssh git

# utilities
sudo pacman -S curl wget jq less fzf

# graphics
sudo pacman -S xorg lightdm lightdm-greeter i3-gaps termite rofi accountsservice ttf-jetbrains-mono noto-fonts

# development
sudo pacman -S node npm yarn docker docker-compose


### Postinstall
# docker
sudo groupadd docker
sudo usermod -aG docker $USER
sudo systemctl enable docker

