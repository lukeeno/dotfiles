### System files
sudo echo "kallister" > /etc/hostname
sudo echo "KEYMAP=uk" > /etc/vconsole.conf
sudo echo "en_GB.UTF-8 UTF-8" > /etc/locale.gen
sudo echo "LANG=en_GB.UTF-8" > /etc/locale.conf
sudo mkdir -p /etc/X11/xorg.conf.d/
sudo touch /etc/X11/xorg.conf.d/90-touchpad.conf
sudo cat > /etc/X11/xorg.conf.d/90-touchpad.conf << EOL
Section "InputClass"
        Identifier "touchpad"
        MatchIsTouchpad "on"
        Driver "libinput"
        Option "Tapping" "on"
	Option "ScrollMethod" "twofinger"
	Option "TappingButtonMap" "lrm"
EndSection
EOL


### Install yay
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

