### Exports
export PATH=$HOME/bin:/usr/local/bin:$PATH
export ZSH="/home/luky/.oh-my-zsh"


### Variables
ZSH_THEME="robbyrussell"
HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"

plugins=(git docker autoupdate zsh-syntax-highlighting zsh-completions)


### Commands
source $ZSH/oh-my-zsh.sh

# from zsh-completions docs
autoload -U compinit && compinit


### Aliases
alias pls="sudo"
alias please="sudo"
# alias murder='rm -rf'
alias rm="gio trash -f"
alias perm="sudo chmod a+x"
alias dc="docker-compose"
alias :q="exit"

